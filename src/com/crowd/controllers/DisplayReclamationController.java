/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crowd.controllers;

import com.crowd.dao.implementatations.reclamationDAO;
import com.crowd.entities.Reclamation;
import com.crowd.test.CrowdTaha;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

/**
 * FXML Controller class
 *
 * @author Taha
 */
public class DisplayReclamationController implements Initializable {

    @FXML
    private ListView<String> listobjet;
    @FXML
    private ListView<String> listtext;
    @FXML
    private TextField searchtxtf;
    @FXML
    private ComboBox<?> searchbycategorie;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        showlistReclamation();

    }

    public void showlistReclamation() {
        Reclamation p = new Reclamation();
        reclamationDAO reclamationDAO = new reclamationDAO();

        List<Reclamation> reclamation = reclamationDAO.displayAllreclamation();
        List<String> rectitre = new ArrayList<String>();
        List<String> rectexte = new ArrayList<String>();

        //iterate through each team and extract the team name
        for (Reclamation r : reclamation) {
            rectitre.add(r.getTITRE());
            rectexte.add(r.getCONTENUE());
        }
        listobjet.setItems(FXCollections.observableArrayList(rectitre));
        listtext.setItems(FXCollections.observableArrayList(rectexte));

    }

    @FXML
    private void handleDelete(ActionEvent event) {

        reclamationDAO iped = new reclamationDAO();
        ObservableList<String> nom;
        Reclamation reclamation = null;
        nom = listobjet.getSelectionModel().getSelectedItems();
        for (String n : nom) {
            if (iped.findreclamationByTITRE(n) == null) {
                reclamation = null;
            } else {
                reclamation = iped.findreclamationByTITRE(n);
            }
        }
        if (reclamation != null) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Suppression !!!");
            alert.setHeaderText("Etes-vous sur de bien vouloir supprimer '" + reclamation.getTITRE() + "'");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                iped.removereclamation(reclamation.getID_RECLAMATION());
                showlistReclamation();

            }

        }
    }
    private Window primaryStage;

    public boolean showreclamationEditDialog(Reclamation reclamation) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(CrowdTaha.class.getResource("editerreclamation.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit Reclamation");
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initStyle(StageStyle.UTILITY);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            editReclamationController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setReclamation(reclamation);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @FXML
    private void hundleEditRec(ActionEvent event) {
        reclamationDAO rd = new reclamationDAO();
        ObservableList<String> nom;
        Reclamation reclamation = null;
        nom = listobjet.getSelectionModel().getSelectedItems();
        for (String n : nom) {
            if (rd.findreclamationByTITRE(n) == null) {
                reclamation = null;
            } else {
                reclamation = rd.findreclamationByTITRE(n);
            }
        }
        if (reclamation != null) {
            boolean modifierClicked = showreclamationEditDialog(reclamation);
            if (modifierClicked) {
                showlistReclamation();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Suppression !!!");
            alert.setHeaderText("selectionnez une reclamation à editer!");

            alert.showAndWait();

        }

    }
     public boolean showreclamationAjoutDialog(Reclamation reclamation) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(CrowdTaha.class.getResource("ajouterreclamation.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit Reclamation");
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initStyle(StageStyle.UTILITY);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            ajoutReclamationController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setReclamation(reclamation);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @FXML
    private void rechercheparnom(ActionEvent event) {
         Reclamation p = new Reclamation();
        reclamationDAO reclamationDAO = new reclamationDAO();

        List<Reclamation> reclamation = reclamationDAO.findlistereclamationByTITRE(searchtxtf.getText());
        List<String> rectitre = new ArrayList<String>();
        List<String> rectexte = new ArrayList<String>();

        //iterate through each team and extract the team name
        for (Reclamation r : reclamation) {
            rectitre.add(r.getTITRE());
            rectexte.add(r.getCONTENUE());
        }
        listobjet.setItems(FXCollections.observableArrayList(rectitre));
        listtext.setItems(FXCollections.observableArrayList(rectexte));
        
    }

    @FXML
    private void hundleAjout(ActionEvent event) {
         reclamationDAO rd = new reclamationDAO();
         Reclamation rec=new Reclamation();
            boolean ajouterClicked = showreclamationAjoutDialog(rec);
        if (ajouterClicked) {
                showlistReclamation();
           
        }

    }
}
