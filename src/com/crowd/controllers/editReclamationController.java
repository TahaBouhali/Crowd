/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.crowd.controllers;

import com.crowd.dao.implementatations.reclamationDAO;
import com.crowd.entities.CategorieRecl;
import com.crowd.entities.Reclamation;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Ben Hmida Mahdi
 */
public class editReclamationController implements Initializable {
    @FXML
    private TextField nomrecTextField;
    @FXML
    private ComboBox<CategorieRecl> categreccombo;
    @FXML
    private TextField idrecTextField;
    @FXML
    private TextArea descrArea;
    @FXML
    private Label nomeditprobLabel;
    
    private boolean modifierClicked = false;
    private Stage dialogStage;
    private Reclamation reclamation;
     public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }   
    public void setReclamation(Reclamation  reclamation){
        this.reclamation=reclamation;
        if(reclamation!=null){
            idrecTextField.setText(Integer.toString(reclamation.getID_RECLAMATION()));
            nomrecTextField.setText(reclamation.getTITRE());
            descrArea.setText(reclamation.getCONTENUE());
           // categreccombo.setItems( FXCollections.observableArrayList( CategorieRecl.values()));
        }
    }

    @FXML
    private void handleEditer(ActionEvent event) {
        reclamation.setID_USER(1);
         int     ID_CATEGORIE_RECLAMATION=0;

        reclamation.setID_USER(1);
                reclamation.setTITRE(nomrecTextField.getText());
//               if(categreccombo.getValue()==CategorieRecl.Reclamation_Evenement){
//                   ID_CATEGORIE_RECLAMATION=1;
//               }else 
//                   if (categreccombo.getValue()==CategorieRecl.Reclamation_Freelance){
//                ID_CATEGORIE_RECLAMATION=2;
//
//                   }else if(categreccombo.getValue()==CategorieRecl.Reclamation_Projet){
//                   ID_CATEGORIE_RECLAMATION=3;
//               }
               reclamation.setID_RECLAMATION(Integer.parseInt(idrecTextField.getText()));
                //reclamation.setID_CATEGORIE_RECLAMATION(ID_CATEGORIE_RECLAMATION);
                reclamation.setTITRE(nomrecTextField.getText());
                reclamation.setCONTENUE(descrArea.getText());
                reclamationDAO recDAO = new reclamationDAO();
                recDAO.updateReclamationbyidreclamation(reclamation, reclamation.getID_RECLAMATION());
                modifierClicked = true;
                
                dialogStage.close();
    }
     public boolean isOkClicked() {
        return modifierClicked;
    }

    @FXML
    private void handleAnnuler(ActionEvent event) {
        dialogStage.close();
    }
    
}
