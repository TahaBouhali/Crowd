/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crowd.dao.implementatations;


import com.crowd.dao.interfaces.Icategorie_reclamationDAO;
import com.crowd.database.DataSource;
import com.crowd.entities.categorie_reclamation;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
/**
 *
 * @author Taha
 */
public class categorie_reclamationDAO implements  Icategorie_reclamationDAO{
      private Connection cnx;

    public categorie_reclamationDAO() {
        cnx= cnx = DataSource.getInstance().getConnection();
    }

    public void add(categorie_reclamation d) {
    String req="insert into reclamation (id,CENTENU) values (?,?)";
        try {
     PreparedStatement ps=cnx.prepareStatement(req);
     ps.setInt(1, d.getId());
      ps.setString(2, d.getCENTENU());
     ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(reclamationDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    
    
    }
      @Override
 public boolean removecategorie_reclamation(int ref) {
        try {
            String requete = "DELETE FROM `reclamation` WHERE ID_categorie_reclamation" + ref + "";
            Statement pst = cnx.createStatement();
            pst.executeUpdate(requete);
            return true;
        } catch (SQLException ex) {
            System.out.println("famma mochkel de delete");
            return false;
        }
    }
    

   

    @Override
    public categorie_reclamation getRelamation(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
     public List<categorie_reclamation> displayAll_categorie_reclamation() {
        List<categorie_reclamation> liste_categorie_reclamations = new ArrayList<categorie_reclamation>();

        String requete = "select * from categorie_reclamation";
        try {
            Statement statement = cnx.createStatement();

            ResultSet resultat = statement.executeQuery(requete);

            while (resultat.next()) {

                categorie_reclamation categorie_reclamation = new categorie_reclamation();

                categorie_reclamation.setId(resultat.getInt("ID_CATEGORIE_RECLAMATION"));
         
                categorie_reclamation.setCENTENU(resultat.getString("CONTENU"));
       

                liste_categorie_reclamations.add(categorie_reclamation);
            }
            System.out.println(liste_categorie_reclamations);
            return liste_categorie_reclamations ;
        } catch (SQLException ex) {
            //Logger.getLogger(PersonneDao.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("erreur lors du chargement des reclamations" + ex.getMessage());
            return null;
        }
    } 
      public List<categorie_reclamation> findreclamationByCategorie( String a) {

        List<categorie_reclamation> liste_categorie_Reclamation = new ArrayList<categorie_reclamation>();
        String requete = "select * from categorie_reclamation where CONTENU='"+a+"'";

        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                categorie_reclamation categorie_reclamation = new categorie_reclamation();

                categorie_reclamation.setId(resultat.getInt("ID_CATEGORIE_RECLAMATION"));
         
                categorie_reclamation.setCENTENU(resultat.getString("CONTENU"));
       
                liste_categorie_Reclamation.add(categorie_reclamation);
        
                
            }
            System.out.println(liste_categorie_Reclamation);
            return liste_categorie_Reclamation;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }
}
    

