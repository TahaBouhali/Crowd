/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.crowd.dao.implementatations;

import com.crowd.dao.interfaces.IReclamationDAO;
import com.crowd.database.DataSource;
import com.crowd.entities.Reclamation;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.sql.ResultSet;
//import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Taha
 */
public class reclamationDAO implements  IReclamationDAO {
    private Connection cnx;

    public reclamationDAO() {
        cnx = DataSource.getInstance().getConnection();
    }

    public void add(Reclamation d) {
    String req="insert into reclamation (ID_RECLAMATION,ID_USER,ID_CATEGORIE_RECLAMATION,TITRE,CONTENU,NOTIFICATION) values (?,?,?,?,?,?)";
        try {
     PreparedStatement ps=cnx.prepareStatement(req);
     ps.setInt(1, d.getID_RECLAMATION());
     ps.setInt(2, d. getID_USER());
      ps.setInt(3, d. getID_CATEGORIE_RECLAMATION());
      ps.setString(4, d. getTITRE());
      ps.setString(5, d. getCONTENUE());
        ps.setInt(6, d. getNOTIFICATION());
     ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(reclamationDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    
    
    }

    public List<Reclamation> display() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

 

 

    @Override
    public Reclamation getReclamation(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     *
     * @param ref
     * @return
     */
    @Override
      public boolean removereclamation(int ref) {
        try {
            String requete = "DELETE FROM `reclamation` WHERE ID_RECLAMATION=" + ref + "";
            Statement pst = cnx.createStatement();
            pst.executeUpdate(requete);
            return true;
        } catch (SQLException ex) {
            System.out.println("famma mochkel de delete");
            return false;
        }
    }
      
  public List<Reclamation> displayAllreclamation() {
        List<Reclamation> listeReclamations = new ArrayList<Reclamation>();

        String requete = "select * from reclamation";
        try {
            Statement statement = cnx.createStatement();

            ResultSet resultat = statement.executeQuery(requete);

            while (resultat.next()) {

                Reclamation Reclamation = new Reclamation();

                Reclamation.setID_RECLAMATION(resultat.getInt("ID_RECLAMATION"));
                Reclamation.setID_USER(resultat.getInt("ID_USER"));
                Reclamation.setID_CATEGORIE_RECLAMATION(resultat.getInt("ID_CATEGORIE_RECLAMATION"));
                Reclamation.setTITRE(resultat.getString("TITRE"));
                Reclamation.setCONTENUE(resultat.getString("CONTENU"));
                Reclamation.setNOTIFICATION(resultat.getInt("NOTIFICATION"));

                listeReclamations.add(Reclamation);
            }
            System.out.println(listeReclamations);
            return listeReclamations;
        } catch (SQLException ex) {
            //Logger.getLogger(PersonneDao.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("erreur lors du chargement des reclamations" + ex.getMessage());
            return null;
        }
    }      

    @Override
   public List<Reclamation> findlistereclamationByTITRE( String a) {

        String requete = "select * from reclamation where TITRE like ?";

        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setString(1,a);
            ResultSet resultat = ps.executeQuery();
            List<Reclamation> lrec=new ArrayList<Reclamation>();
             Reclamation reclamation = new Reclamation();
            while (resultat.next()) {
                              


                reclamation.setID_RECLAMATION(resultat.getInt("ID_RECLAMATION"));
                reclamation.setID_USER(resultat.getInt("ID_USER"));
                reclamation.setID_CATEGORIE_RECLAMATION(resultat.getInt("ID_CATEGORIE_RECLAMATION"));
                reclamation.setTITRE(resultat.getString("TITRE"));
                reclamation.setCONTENUE(resultat.getString("CONTENU"));
                reclamation.setNOTIFICATION(resultat.getInt("NOTIFICATION"));
                lrec.add(reclamation);
                
            }
            if (Objects.nonNull(reclamation)) {
            return lrec;}
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
           
        } return null;
    }
   public boolean updateReclamationbyidreclamation (Reclamation c1, int id) {
        try {
            String requete = "update reclamation set ID_USER=?,TITRE=?,CONTENU=?,NOTIFICATION=? where ID_RECLAMATION=?";
            
            PreparedStatement ps = cnx.prepareStatement(requete);
        
     ps.setInt(1, c1. getID_USER());
      //ps.setInt(2, c1. getID_CATEGORIE_RECLAMATION());
      ps.setString(2, c1. getTITRE());
      ps.setString(3, c1. getCONTENUE());
        ps.setInt(4, c1. getNOTIFICATION());
        ps.setInt(5, id);
     ps.executeUpdate();
            ps.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(reclamationDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public Reclamation findreclamationByTITRE(String a) {
        String requete = "select * from reclamation where TITRE=?";

        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setString(1,a);
            ResultSet resultat = ps.executeQuery();
             Reclamation reclamation = new Reclamation();
            while (resultat.next()) {
                              


                reclamation.setID_RECLAMATION(resultat.getInt("ID_RECLAMATION"));
                reclamation.setID_USER(resultat.getInt("ID_USER"));
                reclamation.setID_CATEGORIE_RECLAMATION(resultat.getInt("ID_CATEGORIE_RECLAMATION"));
                reclamation.setTITRE(resultat.getString("TITRE"));
                reclamation.setCONTENUE(resultat.getString("CONTENU"));
                reclamation.setNOTIFICATION(resultat.getInt("NOTIFICATION"));
                
            }
            if (Objects.nonNull(reclamation)) {
            return reclamation;}
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
           
        } return null;
    
    }
    
}


