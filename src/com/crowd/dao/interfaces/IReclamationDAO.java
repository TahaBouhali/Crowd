/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crowd.dao.interfaces;


import com.crowd.entities.Reclamation;
import java.util.List;

/**
 *
 * @author Taha
 */
public interface IReclamationDAO{
    public void add(Reclamation d);
    public Reclamation getReclamation(int id);
    public List<Reclamation> display();
     public boolean removereclamation(int ref);
     
      public List<Reclamation> displayAllreclamation();
      public Reclamation findreclamationByTITRE( String a);
      public List<Reclamation> findlistereclamationByTITRE( String a);
 public boolean updateReclamationbyidreclamation (Reclamation c1, int id);
  
}